require 'time'

class WeeklyGoals
  def initialize
    @goals = [
      "Remove docker: Finish the load test report comparing the new and the old archicteture",
      "Modularize stylesheet: ",
      "Fix bug: Error running functionals on vagrant",
      "Fix bug: Fix Feedback Form",
      "Fix bug: Minify JS"]
  end

  def rotating_goal
    min = Time.new.min
    idx = min % @goals.length
    @goals[idx]
  end

  def all_goals
    @goals
  end
end
