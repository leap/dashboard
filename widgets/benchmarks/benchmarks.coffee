class Dashing.Benchmarks extends Dashing.Widget

  ready: ->
    # This is fired when the widget is done being rendered

  onData: (data) ->
    # Handle incoming data
    # You can access the html node of this widget with `@node`
    # Example: $(@node).fadeOut().fadeIn() will make the node flash each time data comes in.

  @accessor 'hasBadOutliers', ->
    @get('bad') > 0

  @accessor 'hasGoodOutliers', ->
    @get('good') > 0

  @accessor 'isBenchmarked', ->
    !/NOT/.test(@get('is-benchmarked-status'))

  @accessor 'isNotBenchmarked', ->
    /NOT/.test(@get('is-benchmarked-status'))
