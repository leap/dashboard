class Dashing.Meter extends Dashing.Widget

  @accessor 'value', Dashing.AnimatedValue

  constructor: ->
    super
    @observe 'max', (max) ->
      $(@node).find(".meter").trigger('configure', {'max': max})
    @observe 'min', (min) ->
      $(@node).find(".meter").trigger('configure', {'min': min})

  ready: ->
    meter = $(@node).find(".meter")
    meter.attr("data-bgcolor", meter.css("background-color"))
    meter.attr("data-fgcolor", meter.css("color"))
    meter.knob()
