# LEAP Dashboard
Check out http://shopify.github.com/dashing for more information.

## Setup

Install bundler if required

    gem install bundler

Install app dependencies using bundler:

    bundle install --path=vendor/bundle --deployment

If `bundle install` fails along the way, you're probably missing Xcode command line utilities: `xcode-select --install`

If you're getting `fatal error: 'openssl/ssl.h' file not found` installing `eventmachine` dependency, you can try install it using the following command (make sure you have openssl installed):

    gem install eventmachine -v '1.0.7' --  --with-cppflags='-I/usr/local/opt/openssl/include'

To gather the data, the dashboard needs access to your GoCI account & a GitHub token. You can store them and make them available to the app like so:

    cp credentials_example credentials
    source credentials

For this dashboard you furthermore need nodejs installed, e.g.

    nodenv local 0.10.36

Use dashing to build the dashboard or start it locally.

    ./bin/dashing start

See `dashing --help` for usage.
