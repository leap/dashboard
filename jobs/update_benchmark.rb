require "json"
require "net/http"
require "uri"

outliers_uri = URI.parse("https://benchmarks.leap.se/outliers.json")
is_benchmarked_uri = URI.parse("https://benchmarks.leap.se/is-benchmarked.html")

SCHEDULER.every '5m', :first_in => 0 do |job|
  response = Net::HTTP.get(outliers_uri)
  outliers = JSON.parse(response)

  response = Net::HTTP.get(is_benchmarked_uri)
  benchmarked = response.gsub /<[^>]+>/, ''

  send_event('benchmarks', { "good": outliers["good"].length, "bad": outliers["bad"].length, "is-benchmarked-status": benchmarked})
end
