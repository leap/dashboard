require 'gitlab_stats'


SCHEDULER.every '120s', :first_in => 0 do
  broken_builds = []
  running_builds = []
  started = Time.now

  GitlabStats.projects.each do |proj|
    next if proj[:archived]
    next unless proj[:builds_enabled]
    GitlabStats.add_pipeline_stats_to proj

    puts "#{Time.now} gitlab: #{proj[:name]} (#{proj[:ref]}): #{proj[:status]}"
    unless proj[:status] =~ /^success|running|manual|No builds configured/
      broken_builds << proj
    end
    if proj[:status] == 'running'
      running_builds << proj
    end
  end

  failed = broken_builds.size > 0
  running = running_builds.size > 0

  send_event 'gitlab-builds', failed: failed,
    running: running,
    header: "Gitlab builds",
    broken_builds: broken_builds,
    running_builds: running_builds
  puts "#{Time.now} gitlab: Going through all projects took: #{Time.now - started}."
end
